package com.mathi.recyclerviewsample

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import java.util.*

class MainActivity : AppCompatActivity() {

    internal var androidRecyclerView: RecyclerView? = null
    internal var androidList: MutableList<Android>? = null
    internal var customAdapter: CustomAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        androidRecyclerView = findViewById(R.id.android_recycler_view) as RecyclerView

        androidList = ArrayList<Android>()
        setValues()

        customAdapter = CustomAdapter(this@MainActivity, androidList!!)

        val mLayoutManager = LinearLayoutManager(applicationContext)
        androidRecyclerView!!.layoutManager = mLayoutManager
        androidRecyclerView!!.adapter = customAdapter
    }

    private fun setValues() {
        androidList!!.add(Android("Cupcake", "3", "1.5"))
        androidList!!.add(Android("Donut", "4", "1.6"))
        androidList!!.add(Android("Eclair", "5-7", "2.0 - 2.1"))
        androidList!!.add(Android("Froyo", "8", "2.2 - 2.2.3"))
        androidList!!.add(Android("Gingerbread", "9 - 10", "2.3 - 2.3.7"))
        androidList!!.add(Android("Honeycomb", "11 - 13", "3.0 - 3.2.6"))
        androidList!!.add(Android("Ice Cream Sandwich", "14 - 15", "4.0 - 4.0.4"))
        androidList!!.add(Android("Jelly Bean", "16 - 18", "4.1 - 4.3.1"))
        androidList!!.add(Android("Kitkat", "19 - 20", "4.4 - 4.4.4"))
        androidList!!.add(Android("Lollipop", "21 - 22", "5.0 - 5.1.1"))
        androidList!!.add(Android("Marshmallow", "23", "6.0 - 6.0.1"))
        androidList!!.add(Android("Nougat", "24 - 25", "7.0 - 7.1.2"))
    }
}
